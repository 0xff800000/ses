# Question 1 : Configure a secure kernel

A security feature that is easy to check is the *dmesg* restriction.

make linux-xconfig : Security options -> [*] Restrict unprivileged access to the kernel syslog

After this modification, any non-root user will receive the following message :

"""
$ dmesg
dmesg: klogctl: Operation not permitted
"""

# Question 2 : Improve kernel security during the startup

After initializing the kernel parameters, the board did not respond to broadcast pings as expected. That means that the configuration was correctly loaded.

"""
[lmi@localhost tp2]$ ping -b 192.168.0.14
PING 192.168.0.14 (192.168.0.14) 56(84) bytes of data.
64 bytes from 192.168.0.14: icmp_seq=1 ttl=64 time=2.58 ms
64 bytes from 192.168.0.14: icmp_seq=2 ttl=64 time=1.01 ms
64 bytes from 192.168.0.14: icmp_seq=3 ttl=64 time=1.03 ms
--- 192.168.0.14 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2154ms
rtt min/avg/max/mdev = 1.012/1.541/2.582/0.737 ms
[lmi@localhost tp2]$ ping -b 192.168.0.255
WARNING: pinging broadcast address
PING 192.168.0.255 (192.168.0.255) 56(84) bytes of data.
--- 192.168.0.255 ping statistics ---
15 packets transmitted, 0 received, 100% packet loss, time 14542ms
"""

# Question 3: CONFIG_FORTIFY_SOURCE option

"""
[lmi@localhost build]$ grep -rnw . -e CONFIG_FORTIFY_SOURCE
./linux-4.18.5/include/generated/autoconf.h:96:#define CONFIG_FORTIFY_SOURCE 1
./linux-4.18.5/include/config/auto.conf:94:CONFIG_FORTIFY_SOURCE=y
./linux-4.18.5/include/linux/string.h:238:#if !defined(__NO_FORTIFY) && defined(__OPTIMIZE__) && defined(CONFIG_FORTIFY_SOURCE)
./linux-4.18.5/.config:3838:CONFIG_FORTIFY_SOURCE=y
./linux-4.18.5/.config.old:3829:CONFIG_FORTIFY_SOURCE=y
./linux-4.18.5/arch/s390/include/asm/string.h:75:#if !defined(IN_ARCH_STRING_C) && (!defined(CONFIG_FORTIFY_SOURCE) || defined(__NO_FORTIFY))
./linux-4.18.5/arch/s390/configs/debug_defconfig:654:CONFIG_FORTIFY_SOURCE=y
./linux-4.18.5/arch/x86/include/asm/string_64.h:35:#ifndef CONFIG_FORTIFY_SOURCE
./linux-4.18.5/arch/x86/include/asm/string_64.h:48:#endif /* !CONFIG_FORTIFY_SOURCE */
./linux-4.18.5/arch/x86/include/asm/string_32.h:148:#ifndef CONFIG_FORTIFY_SOURCE
./linux-4.18.5/arch/x86/include/asm/string_32.h:192:#endif /* !CONFIG_FORTIFY_SOURCE */
./linux-4.18.5/arch/x86/include/asm/string_32.h:198:#ifndef CONFIG_FORTIFY_SOURCE
./linux-4.18.5/arch/x86/include/asm/string_32.h:323:#ifndef CONFIG_FORTIFY_SOURCE
./linux-4.18.5/arch/x86/include/asm/string_32.h:333:#endif /* !CONFIG_FORTIFY_SOURCE */
./linux-4.18.5/arch/x86/lib/memcpy_32.c:10:#if defined(CONFIG_X86_USE_3DNOW) && !defined(CONFIG_FORTIFY_SOURCE)
./linux-4.18.5/arch/Kconfig:237:	  build and run with CONFIG_FORTIFY_SOURCE.
"""
