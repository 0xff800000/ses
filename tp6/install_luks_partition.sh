#!/bin/bash

# The file ~/workspace/xu3/buildroot/board/hardkernel/xu3/boot.cmd
# must be modified : fatload -> ext4 load
# Then uboot must be recompiled : make uboot-rebuild
device=$1

# umount device
umount $device*

# init 64MB to 0
sudo dd if=/dev/zero of=$device bs=4k count=12000
sync

# First sector : msdos
sudo parted $device mklabel msdos 

# 1st partition : 64MB
sudo parted $device mkpart primary ext4 32768s 163839s

# 2nd partition : 1GB
sudo parted $device mkpart primary ext4 163840s 2260991s

# Format vfat 1st and 2nd partition
sudo mkfs.ext4 $device\1
sudo mkfs.ext4 $device\2 -L rootfs

# Create partition 3 of 1GB
(
echo n # new partition
echo p # primary partition
echo 3 # partition number 1
echo 2260992 # first sector 
echo +1G # 1 GB 
echo p # print the in-memory partition table
echo w # write the partition table
echo q # and we're done
) | sudo fdisk $device

sync

# copy firmware & bl1.bin, bl2.bin, tzsw.bin
sudo dd if=~/workspace/xu3/buildroot/board/hardkernel/xu3/bl1.bin.hardkernel of=$device bs=512 seek=1
sudo dd if=~/workspace/xu3/buildroot/board/hardkernel/xu3/bl2.bin.hardkernel.1mb_uboot of=$device bs=512 seek=31
sudo dd if=~/workspace/xu3/buildroot/board/hardkernel/xu3/tzsw.bin.hardkernel of=$device bs=512 seek=2111

# copy u-boot
sudo dd if=~/workspace/xu3/buildroot/output/images/u-boot.bin of=$device bs=512 seek=63

# copy kernel & flattened device tree
sudo mount $device\1 /mnt
sudo cp ~/workspace/xu3/buildroot/output/images/zImage /mnt
sudo cp ~/workspace/xu3/buildroot/output/images/exynos5422-odroidxu3.dtb /mnt
sudo cp ~/workspace/xu3/buildroot/output/images/boot.scr /mnt
sync
sudo umount $device\1
sudo e2label $device\1 BOOT

# copy rootfs
sudo dd if=~/workspace/xu3/buildroot/output/images/rootfs.ext4 of=$device\2
sudo e2fsck -f $device\2
sudo resize2fs $device\2
sudo e2label $device\2 rootfs

# Init luks
#cryptsetup --debug luksFormat /dev/mmcblk1p3
