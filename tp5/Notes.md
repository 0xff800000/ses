# Build Openssh
## Get the source code and the key

wget https://ftp.spline.de/pub/OpenBSD/OpenSSH/portable/openssh-7.9p1.tar.gz
wget https://ftp.spline.de/pub/OpenBSD/OpenSSH/portable/openssh-7.9p1.tar.gz.asc

## Check the files

gpg --keyserver hkp://keys.gnupg.net --recv-keys 6D920D30
gpg --verify openssh-7.9p1.tar.gz.asc

## Configure compilation for x86 platform 

./configure --prefix=/home/lmi/install CFLAGS="-fstack-protector-all"

## Check if the programs are stripped

objdump -t ssh | less

If the command above is returning some values, the programs can be stripped manually :

strip ssh

# Configure sshd

## Disable Ipv6

echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
echo 1 > /proc/sys/net/ipv6/conf/default/disable_ipv6

## Disable port forward in system

echo 0 > cat /proc/sys/net/ipv4/conf/all/accept_source_route

echo 0 > /proc/sys/net/ipv4/conf/all/forwarding
echo 0 > /proc/sys/net/ipv6/conf/all/forwarding
echo 0 > /proc/sys/net/ipv4/conf/all/mc_forwarding
echo 0 > /proc/sys/net/ipv6/conf/all/mc_forwarding

echo 0 > /proc/sys/net/ipv4/conf/all/accept_redirects
echo 0 > /proc/sys/net/ipv6/conf/all/accept_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/secure_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects

echo 1 > /proc/sys/net/ipv4/conf/default/rp_filter

## SSHd config file


ForwardAgent no
AddressFamily inet
Ciphers aes256-cbc, aes256-ctr, aes128-cbc, hmac-sha256, hmac-sha1
PermitRootLogin no
UsePrivilegeSeparation yes
Banner /some/path

## Configure banner

echo Banner > /etc/issue.net
